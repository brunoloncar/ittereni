﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using WebApp.Models.Utilities;

[Serializable]
public class Rezervacija
{
    public Rezervacija()
    {
        VrijemeRezerviranja = DateTime.UtcNow;
    }

    [Display(Name = "Reservation ID")]
    public Guid RezervacijaId { get; set; } = Guid.NewGuid();

    [Display(Name = "Name and surname")]
    [Required(ErrorMessage = "Please insert your name and surname.")]
    public String ImeIPrezime { get; set; }

    [Display(Name = "Contact number")]
    [Required(ErrorMessage = "Please insert your contact number.")]
    public String BrojMobitela { get; set; }

    [Display(Name = "Email address")]
    [Required(ErrorMessage = "Please insert your email address.")]
    public String Email { get; set; }

    [Column(TypeName = "date")]
    [DisplayFormat(DataFormatString = "{0:dd.MM.yyyy}")]
    [Display(Name = "Reservation date")]
    [Required(ErrorMessage = "Please select reservation date.")]
    public DateTime DatumTermina { get; set; }

    [DisplayFormat(DataFormatString = "{00:##}")]
    [Display(Name = "Available times")]
    [Required(ErrorMessage = "Please select time slot.")]
    public int VrijemeTermina { get; set; }

    [Display(Name = "Duration")]
    [Required(ErrorMessage = "Please select duration.")]
    public int TrajanjeTerminaUSatima { get; set; }

    [Display(Name = "Court")]
    [Required(ErrorMessage = "Please select court.")]
    public string Teren { get; set; }
    
    [Display(Name = "Reserved at")]
    public DateTime VrijemeRezerviranja { get; set; }

    public bool Otkazano { get; set; } = false;

    public String Notes { get; internal set; }

    [Display(Name = "Canceled at")]
    public DateTime? VrijemeOtkazivanja { get; set; } = null;

    public bool OsobniPodatciIzbrisani { get; set; } = false;

    public bool Occupied { get; internal set; }

    public override string ToString()
    {
        return ImeIPrezime;
    }
}

