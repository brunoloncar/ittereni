﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;


public class Termin
{
    public int TerminSat { get; set; }
    public String SatDisplayFormat { get; set; }

    public Termin(int terminsat)
    {
        TerminSat = terminsat;
        SatDisplayFormat = GetDisplayForTime(terminsat);
    }

    private string GetDisplayForTime(int terminsat)
    {
        if (terminsat < 10)
        {
            return $"0{terminsat}:00";
        }
        return $"{terminsat}:00";

    }

    public override bool Equals(object obj)
    {
        if (!(obj is Termin))
        {
            return false;
        }
        return TerminSat == (obj as Termin).TerminSat;
    }

    public override int GetHashCode()
    {
        return base.GetHashCode();
    }
}
