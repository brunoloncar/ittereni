﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApp.Models.Utilities
{
    public class Pozadina
    {
        public Pozadina(string naziv)
        {
            Naziv = naziv;
        }

        public String Naziv { get; set; }
        public String URL {
            get
            {
                return $"{Naziv}.png";
            }
        }
    }
}