﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

public class Trajanje
{
    public int TrajanjeUSatima { get; set; }
    public String TrajanjeDisplayFormat { get; set; }

    public Trajanje(int TrajanjeUSatima)
    {
        this.TrajanjeUSatima = TrajanjeUSatima;
        TrajanjeDisplayFormat = GetDisplayForTrajanje(TrajanjeUSatima);
    }

    private string GetDisplayForTrajanje(int trajanjeUSatima)
    {
        if (trajanjeUSatima == 1)
        {
            return "1 hour";
        }
        else
        {
            return $"{trajanjeUSatima} hours";
        }
    }


}
