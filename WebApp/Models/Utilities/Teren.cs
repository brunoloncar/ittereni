﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;

namespace WebApp.Models.Utilities
{
    public class Teren
    {
        private static readonly char DELIMITER = '|';

        public Teren(string naziv)
        {
            Naziv = naziv;
        }

        public int TerenId { get; set; }

        public string Naziv { get; set; }

        internal static IEnumerable<Teren> ParseTereniFromConfigFile(string property)
        {
            //checks if teren name is not empty string
            foreach (var item in property.Split(DELIMITER).Where(x => x.Length > 0).ToList())
            {
                yield return new Teren(item);
            }
        }

        internal static string FormatTereniForConfigFile(IEnumerable<Teren> tereni)
        {
            var stringBuilder = new StringBuilder();
            foreach (var item in tereni)
            {
                stringBuilder.Append(item);
                stringBuilder.Append(DELIMITER);
            }
            return stringBuilder.ToString();
        }

        public override string ToString()
        {
            return Naziv;
        }

        public string Vrijednost(int i) => $"teren{i}";

        public override bool Equals(object obj)
        {
            if (obj == null)
            {
                return false;
            }
            return (obj as Teren).Naziv == Naziv;
        }

        public override int GetHashCode()
        {
            return 1517740225 + EqualityComparer<string>.Default.GetHashCode(Naziv);
        }
    }
}