﻿namespace WebApp.Models.Utilities
{
    public enum DanUTjednu
    {
        Ponedjeljak,
        Utorak,
        Srijeda,
        Četvrtak,
        Petak,
        Subota,
        Nedjelja
    }
}