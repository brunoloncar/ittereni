﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using WebApp.Models.Utilities;

namespace WebApp.Models
{
    public class PodatciOTerenima
    {
        public int PodatciOTerenimaId { get; set; }
        
        [Display(Name = "Short name")]
        public String AppShortName { get; set; }

        [Display(Name = "Long name")]
        public String AppLongName { get; set; }

        [Display(Name = "Contact phone")]
        public String KontaktMobitel { get; set; }

        [Display(Name = "Contact email")]
        public String KontaktEmail { get; set; }

        [Display(Name = "Address")]
        public String AdresaObjekta { get; set; }

        [Display(Name = "Description")]
        public String OpisObjekta { get; set; }

        [Range(minimum: 1, maximum: 23)]
        [Display(Name = "Working hours from")]
        public int RadnoVrijemeOd { get; set; }

        [Range(minimum: 1, maximum: 23)]
        [Display(Name = "Working hours to")]
        public int RadnoVrijemeDo { get; set; }

        [Range(minimum: 1, maximum: 100000)]
        [Display(Name = "Slideshow speed")]
        public int BrzinaCarousela { get; set; }

        [Display(Name = "Background image")]
        public String SlikaPozadineSadrzaja { get; set; }

        [Display(Name = "Summer (from/to)")]
        public String LjetoDatumiOdDo { get; set; }

        [Display(Name = "Winter (from/to)")]
        public String ZimaDatumiOdDo { get; set; }

        [Range(minimum: 0, maximum: int.MaxValue)]
        [Display(Name = "Price (summer/day)")]
        public int CijenaLjetoDan { get; set; }

        [Range(minimum: 0, maximum: int.MaxValue)]
        [Display(Name = "Price (summer/night)")]
        public int CijenaLjetoNoc { get; set; }

        [Range(minimum: 0, maximum: int.MaxValue)]
        [Display(Name = "Price (winter/day)")]
        public int CijenaZimaDan { get; set; }

        [Range(minimum: 0, maximum: int.MaxValue)]
        [Display(Name = "Price (winter/night)")]
        public int CijenaZimaNoc { get; set; }
    }
}