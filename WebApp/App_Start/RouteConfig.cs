﻿using System.Web.Mvc;
using System.Web.Routing;
using WebApp.Controllers;

namespace WebApp
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");
            routes.LowercaseUrls = true;
            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "home", action = nameof(HomeController.Index), id = UrlParameter.Optional }
            );
        }
    }
}
