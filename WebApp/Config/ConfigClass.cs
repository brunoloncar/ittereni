﻿using System;
using System.IO;
using System.Text.RegularExpressions;

namespace WebApp.Config
{
    public class ConfigClass
    {
        public static string ConfigFileURL { get; } = AppDomain.CurrentDomain.BaseDirectory + @"Config/config.txt";

        public static String GetProperty(String propertyKey)
        {
            Regex regex = new Regex($"{propertyKey}=\"(.+)\"");
            using (StreamReader reader = new StreamReader(ConfigFileURL))
            {
                string line;
                while ((line = reader.ReadLine()) != null)
                {
                    Match match = regex.Match(line);
                    if (match.Success)
                    {
                        return match.Groups[1].Value;
                    }
                }
                throw new Exception($"Property not found ({propertyKey}).");
            }
        }

        public static void SetProperty(String propertyKey, String newValue)
        {
            File.WriteAllText(
                ConfigClass.ConfigFileURL, Regex.Replace(
                    File.ReadAllText(ConfigClass.ConfigFileURL), 
                    $"{propertyKey}=\"(.+)\"", $"{propertyKey}=\"{newValue}\""));
        }
    }
}