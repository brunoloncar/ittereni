﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using WebApp.Models;

namespace WebApp.Data
{
    public class TennisContext : DbContext
    {
        public TennisContext() : base("DefaultConnection")
        {
        }

        public DbSet<Rezervacija> Rezervacijas { get; set; }
    }
}