﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.Entity;
using System.Data.Entity.Validation;
using System.Linq;
using System.Net.Mail;
using WebApp.Models;
using WebApp.Config;
using WebApp.Models.Utilities;
using System.Text;
using System.IO;
using System.Web;

namespace WebApp.Data
{
    public class Repo
    {

        //---Reservations---

        //CRUD
        internal static void AddRezervacija(Rezervacija rezervacija)
        {

            if (rezervacija == null)
            {
                return;
            }

            using (var db = new TennisContext())
            {
                if (Repo.GetRezervacijeZaSat(rezervacija.DatumTermina, rezervacija.VrijemeTermina).Count >= Teren.ParseTereniFromConfigFile(ConfigClass.GetProperty("tereni")).ToList().Count)
                {
                    throw new Exception("Termin nije dostupan.");
                }

                db.Rezervacijas.Add(rezervacija);
                db.SaveChanges();

                Repo.SendRezerviranoEmailToAdmins(rezervacija);
                if (rezervacija.Email != null)
                {
                    Repo.SendRezerviranoEmailToUser(rezervacija.Email, rezervacija);
                }
            }
        }

        internal static Rezervacija GetRezervacija(Guid idRezervacija)
        {
            using (var db = new TennisContext())
            {
                //return db.AppUsers.Include(x => x.Reservations).SingleOrDefault(b => b.AppUserId == idAppUser);
                return db.Rezervacijas.SingleOrDefault(b => b.RezervacijaId == idRezervacija);
            }

        }

        internal static void UpdateReservation(Rezervacija reservation, Guid guid)
        {
            Rezervacija _reservation;
            using (var db = new TennisContext())
            {
                _reservation = db.Rezervacijas.Where(r => r.RezervacijaId == guid).FirstOrDefault<Rezervacija>();
            }

            if (_reservation != null)
            {
                _reservation.ImeIPrezime = reservation.ImeIPrezime;
                _reservation.BrojMobitela = reservation.BrojMobitela;
                _reservation.Email = reservation.Email;
                _reservation.Notes = reservation.Notes;
                _reservation.VrijemeTermina = reservation.VrijemeTermina;
                _reservation.DatumTermina = reservation.DatumTermina;
                _reservation.Otkazano = reservation.Otkazano;
                _reservation.Teren = reservation.Teren;
            }

            using (var db = new TennisContext())
            {
                db.Entry(_reservation).State = System.Data.Entity.EntityState.Modified;

                db.SaveChanges();
            }
        }

        internal static void OtkaziRezervaciju(Rezervacija rezervacija)
        {
            //return if reservation is null
            if (rezervacija == null)
            {
                return;
            }

            //send mails
            SendOtkazanoEmailToAdmins(rezervacija);
            if (rezervacija.Email != null)
            {
                SendOtkazanoEmailToUser(rezervacija.Email, rezervacija);
            }

            //cancellation logic
            using (var db = new TennisContext())
            {
                var std = db.Rezervacijas.Find(rezervacija.RezervacijaId);
                if (rezervacija.ImeIPrezime == "Zauzeto")
                {
                    db.Rezervacijas.Remove(std);
                    db.SaveChanges();
                }
                else
                {
                    std.Otkazano = true;
                    std.VrijemeOtkazivanja = DateTime.UtcNow;
                    db.SaveChanges();
                }
            }
        }



        //Get reservations

        internal static List<Rezervacija> GetRezervacijeZaSat(DateTime datum, int sat)
        {
            using (var db = new TennisContext())
            {
                return db.Rezervacijas
                    .Where(c => DbFunctions.TruncateTime(c.DatumTermina) == datum.Date && c.VrijemeTermina == sat && c.Otkazano == false)
                    .ToList();
            }
        }

        internal static IEnumerable<Rezervacija> GetPosljednjeRezervacije(int howMany)
        {
            using (var db = new TennisContext())
            {
                var rezervacije = db.Rezervacijas
                    .Where(c => c.OsobniPodatciIzbrisani == false && c.Otkazano == false)
                    .OrderByDescending(c => c.VrijemeRezerviranja)
                    .Take(howMany)
                    .ToList();

                foreach (var item in rezervacije)
                {
                    yield return item;
                }
            }
        }

        internal static IEnumerable<Rezervacija> GetRezervacijeZaDan(DateTime datumTermina)
        {
            using (var db = new TennisContext())
            {
                var rezervacije = db.Rezervacijas
                    .Where(c => DbFunctions.TruncateTime(c.DatumTermina) == datumTermina && c.Otkazano == false)
                    .OrderBy(c => c.VrijemeTermina)
                    .ThenBy(c => c.VrijemeRezerviranja)
                    .ToList();

                foreach (var item in rezervacije)
                {
                    yield return item;
                }
            }
        }

        internal static IEnumerable<Rezervacija> GetRezervacijeZaRangeDana(DateTime datumOd, DateTime datumDo)
        {
            using (var db = new TennisContext())
            {
                var rezervacije = db.Rezervacijas
                    .Where(c => DbFunctions.TruncateTime(c.DatumTermina) >= datumOd && DbFunctions.TruncateTime(c.DatumTermina) <= datumDo && c.Otkazano == false)
                    .OrderBy(c => c.DatumTermina)
                    .ThenBy(c => c.VrijemeTermina)
                    .ThenBy(c => c.VrijemeRezerviranja)
                    .ToList();

                foreach (var item in rezervacije)
                {
                    yield return item;
                }
            }
        }

        //Canceled reservations
        internal static IEnumerable<Rezervacija> GetOtkazaneRezervacijeZaDan(DateTime datumTermina)
        {
            using (var db = new TennisContext())
            {
                var rezervacije = db.Rezervacijas
                    .Where(c => DbFunctions.TruncateTime(c.DatumTermina) == datumTermina && c.Otkazano == true)
                    .OrderBy(c => c.VrijemeTermina)
                    .ThenBy(c => c.VrijemeRezerviranja)
                    .ToList();

                foreach (var item in rezervacije)
                {
                    yield return item;
                }
            }
        }

        internal static IEnumerable<Rezervacija> GetOtkazaneRezervacijeZaDan(DateTime datumOd, DateTime datumDo)
        {
            using (var db = new TennisContext())
            {
                var rezervacije = db.Rezervacijas
                    .Where(c => DbFunctions.TruncateTime(c.DatumTermina) >= datumOd && DbFunctions.TruncateTime(c.DatumTermina) <= datumDo && c.Otkazano == true)
                    .OrderBy(c => c.VrijemeTermina)
                    .ThenBy(c => c.VrijemeRezerviranja)
                    .ToList();

                foreach (var item in rezervacije)
                {
                    yield return item;
                }
            }
        }

        //Reserved reservations
        internal static void AddZauzetoRezervacija(DateTime datum, int vrijeme, string teren)
        {
            AddZauzetoRezervacija(datum, vrijeme, teren, null);
        }

        internal static String AddZauzetoRezervacija(DateTime datum, int vrijeme, string teren, string notes)
        {
            using (var db = new TennisContext())
            {
                var rezervacija = new Rezervacija()
                {
                    ImeIPrezime = "Occupied",
                    Occupied = true,
                    Email = "noemail@occupied.com",
                    BrojMobitela = "0000000",
                    DatumTermina = datum,
                    VrijemeTermina = vrijeme,
                    Teren = teren,
                    TrajanjeTerminaUSatima = 1
                };
                if (notes != null)
                {
                    rezervacija.Notes = notes;
                }
                db.Rezervacijas.Add(rezervacija);
                db.SaveChanges();
                return $"{rezervacija.DatumTermina.ToString(ConfigClass.GetProperty("dateFormat"))} " +
                    $"{new Termin(rezervacija.VrijemeTermina).SatDisplayFormat} ({rezervacija.Teren}) -> Booked";
            }
            //dodaje rezervaciju koja se stvori sa zauzećem (DodajTermin)
        }



        //---Termini---
        private static IEnumerable<Termin> GetSviTermini()
        {
            var radnoVrijemeOd = int.Parse(ConfigClass.GetProperty("radnoVrijemeOd"));
            var radnoVrijemeDo = int.Parse(ConfigClass.GetProperty("radnoVrijemeDo"));

            for (int i = radnoVrijemeOd; i <= radnoVrijemeDo; i++)
            {
                yield return new Termin(i);
            }
        }

        internal static IEnumerable<Termin> GetSlobodniTermini(DateTime date, int trajanje = 1)
        {
            using (var db = new TennisContext())
            {
                var sviTermini = GetSviTermini().ToList();

                //fetch reservations for the date
                var sveRezervacijeZaDan = db.Rezervacijas
                    .Where(c => DbFunctions.TruncateTime(c.DatumTermina) == date && c.Otkazano == false)
                    .ToList();


                var zauzetiTermini = new List<Rezervacija>();
                foreach (var item in sveRezervacijeZaDan)
                {
                    var tList = Repo.GetRezervacijeZaSat(date, item.VrijemeTermina);
                    if (tList.Count >= Teren.ParseTereniFromConfigFile(ConfigClass.GetProperty("tereni")).ToList().Count)
                    {
                        zauzetiTermini.Add(item);
                    }
                }

                //makni sve zauzete termine
                foreach (var item in zauzetiTermini)
                {
                    sviTermini.Remove(new Termin(item.VrijemeTermina));
                }

                //ako je termin danas makni sve protekle sate
                if (date == DateTime.UtcNow && DateTime.UtcNow.Hour != 24)
                {
                    var termini2 = new List<Termin>(sviTermini);
                    foreach (var item in termini2)
                    {
                        if (item.TerminSat <= DateTime.UtcNow.Hour) ///
                        {
                            sviTermini.Remove(item);
                        }
                    }
                }

                //if date is set to before today
                if (date < DateTime.Today)
                {
                    sviTermini.Clear();
                }

                //vrati sve termine
                foreach (var item in sviTermini)
                {
                    yield return item;
                }
            }
        }



        //---Util---
        internal static IEnumerable<Trajanje> GetTrajanja()
        {
            //returns only Trajanje 1 hour

            var list = new List<Trajanje>
            {
                new Trajanje(1)
            };

            foreach (var item in list)
            {
                yield return item;
            }
        }



        //---Emails---
        private static void SendEmailToAdresses(IList<string> adresses, string mailMessage, string mailSubject)
        {
            try
            {
                MailMessage mail = new MailMessage();
                SmtpClient SmtpServer = new SmtpClient(ConfigClass.GetProperty("SmtpServer"));

                mail.From = new MailAddress(ConfigClass.GetProperty("FromAddress"));
                foreach (var item in adresses)
                {
                    mail.To.Add(item);
                }
                mail.Subject = mailSubject;
                mail.Body = mailMessage;
                mail.IsBodyHtml = true;
                mail.BodyEncoding = System.Text.Encoding.UTF8;

                SmtpServer.Port = 587;
                SmtpServer.Credentials = new System.Net.NetworkCredential(
                    ConfigClass.GetProperty("SmtpUsername"),
                    ConfigClass.GetProperty("SmtpPassword"));
                SmtpServer.EnableSsl = true;

                SmtpServer.Send(mail);
            }
            catch (Exception)
            {
            }
        }

        private static void SendRezerviranoEmailToAdmins(Rezervacija rezervacija)
        {
            IList<String> adresses = new List<String>(){
                ConfigClass.GetProperty("MailToSendNotificationsTo")
            };
            string url = ConfigClass.GetProperty("DomainAddress") + ConfigClass.GetProperty("PathToOtkaziRetervaciju") + rezervacija.RezervacijaId;
            string message = $"<h2 style=\"color: darkgreen;\">Pristigla je nova rezervacija!</h2><hr />" +
                $"<h3>Detalji rezervacije:</h3><b>Ime i prezime: </b> {rezervacija.ImeIPrezime}" +
                $"<br /><b>Broj mobitela: </b> {rezervacija.BrojMobitela}" +
                $"<br /><b>Datum termina: </b> {rezervacija.DatumTermina.ToString("dd.MM.yyyy")}" +
                $"<br /><b>Vrijeme termina: </b> {new Termin(rezervacija.VrijemeTermina).SatDisplayFormat}" +
                $"<br /><b>Teren: </b> {rezervacija.Teren}" +
                $"<br /><b>Trajanje: </b> {rezervacija.TrajanjeTerminaUSatima}h" +
                $"<br /><b>ID rezervacije: </b> {rezervacija.RezervacijaId}" +
                $"<br /><br /><hr />" +
                $"<a href=\"{url}\">Otkaži rezervaciju</>";
            string mailSubject = $"TT: Nova rezervacija za {rezervacija.DatumTermina.ToString("dd.MM.yyyy")} u {new Termin(rezervacija.VrijemeTermina).SatDisplayFormat}";
            SendEmailToAdresses(adresses, message, mailSubject);
        }

        private static void SendRezerviranoEmailToUser(string email, Rezervacija rezervacija)
        {
            IList<String> adresses = new List<String>(){
                email
            };
            string url = ConfigClass.GetProperty("DomainAddress") + ConfigClass.GetProperty("PathToOtkaziRetervaciju") + rezervacija.RezervacijaId;
            string message = $"<h2 style=\"color: darkgreen;\">Vaša rezervacija za Teniske terene Klaka je uspješna!</h2><hr />" +
                $"<h3>Detalji rezervacije:</h3><b>Ime i prezime: </b> {rezervacija.ImeIPrezime}" +
                $"<br /><b>Broj mobitela: </b> {rezervacija.BrojMobitela}" +
                $"<br /><b>Datum termina: </b> {rezervacija.DatumTermina.ToString("dd.MM.yyyy")}" +
                $"<br /><b>Vrijeme termina: </b> {new Termin(rezervacija.VrijemeTermina).SatDisplayFormat}" +
                $"<br /><b>Teren: </b> {rezervacija.Teren}" +
                $"<br /><b>Trajanje: </b> {rezervacija.TrajanjeTerminaUSatima}h" +
                $"<br /><b>ID rezervacije: </b> {rezervacija.RezervacijaId}" +
                $"<br /><br /><hr />" +
                $"<a href=\"{url}\">Otkaži rezervaciju</>";
            string mailSubject = $"TT-Klaka: Uspješna rezervacija za {rezervacija.DatumTermina.ToString("dd.MM.yyyy")}";
            SendEmailToAdresses(adresses, message, mailSubject);
        }

        private static void SendOtkazanoEmailToAdmins(Rezervacija rezervacija)
        {
            IList<String> adresses = new List<String>(){
                ConfigClass.GetProperty("MailToSendNotificationsTo")
            };
            string message = $"<h2 style=\"color: darkred;\">Otkazana rezervacija!</h2><hr />" +
                $"<h3>Detalji rezervacije:</h3><b>Ime i prezime: </b> {rezervacija.ImeIPrezime}" +
                $"<br /><b>Broj mobitela: </b> {rezervacija.BrojMobitela}" +
                $"<br /><b>Datum termina: </b> {rezervacija.DatumTermina.ToString("dd.MM.yyyy")}" +
                $"<br /><b>Vrijeme termina: </b> {new Termin(rezervacija.VrijemeTermina).SatDisplayFormat}" +
                $"<br /><b>Teren: </b> {rezervacija.Teren}" +
                $"<br /><b>Trajanje: </b> {rezervacija.TrajanjeTerminaUSatima}h" +
                $"<br /><b>ID rezervacije: </b> {rezervacija.RezervacijaId}" +
                $"<br /><br /><hr />" +
                $"<a href={ConfigClass.GetProperty("DomainAddress")}>Teniski tereni Klaka</>";
            string mailSubject = $"TT: Otkazana rezervacija za {rezervacija.DatumTermina.ToString("dd.MM.yyyy")}";
            SendEmailToAdresses(adresses, message, mailSubject);
        }

        private static void SendOtkazanoEmailToUser(string email, Rezervacija rezervacija)
        {
            IList<String> adresses = new List<String>(){
                email
            };
            string message = $"<h2 style=\"color: darkred;\">Otkazana rezervacija!</h2><hr />" +
                $"<h3>Detalji rezervacije:</h3><b>Ime i prezime: </b> {rezervacija.ImeIPrezime}" +
                $"<br /><b>Broj mobitela: </b> {rezervacija.BrojMobitela}" +
                $"<br /><b>Datum termina: </b> {rezervacija.DatumTermina.ToString("dd.MM.yyyy")}" +
                $"<br /><b>Vrijeme termina: </b> {new Termin(rezervacija.VrijemeTermina).SatDisplayFormat}" +
                $"<br /><b>Teren: </b> {rezervacija.Teren}" +
                $"<br /><b>Trajanje: </b> {rezervacija.TrajanjeTerminaUSatima}h" +
                $"<br /><b>ID rezervacije: </b> {rezervacija.RezervacijaId}" +
                $"<br /><br /><hr />" +
                $"<a href={ConfigClass.GetProperty("DomainAddress")}>Teniski tereni Klaka</>";
            string mailSubject = $"TT-Klaka: Otkazana rezervacija za {rezervacija.DatumTermina.ToString("dd.MM.yyyy")}";
            SendEmailToAdresses(adresses, message, mailSubject);
        }




        //---Podatci o terenima---
        internal static void UrediPodatkeOTerenima(PodatciOTerenima podatci)
        {
            Config.ConfigClass.SetProperty("appShortName", podatci.AppShortName);
            Config.ConfigClass.SetProperty("appLongName", podatci.AppLongName);
            Config.ConfigClass.SetProperty("kontaktMobitel", podatci.KontaktMobitel);
            Config.ConfigClass.SetProperty("kontaktEmail", podatci.KontaktEmail);
            Config.ConfigClass.SetProperty("adresaObjekta", podatci.AdresaObjekta);
            Config.ConfigClass.SetProperty("courtsAddress1", podatci.OpisObjekta);

            //Config.ConfigClass.SetProperty("tereni", Teren.FormatTereniForConfigFile(podatci.Tereni));
            Config.ConfigClass.SetProperty("radnoVrijemeOd", podatci.RadnoVrijemeOd.ToString());
            Config.ConfigClass.SetProperty("radnoVrijemeDo", podatci.RadnoVrijemeDo.ToString());
            Config.ConfigClass.SetProperty("brzinaCarousela", podatci.BrzinaCarousela.ToString());
            Config.ConfigClass.SetProperty("slikaPozadineSadrzaja", podatci.SlikaPozadineSadrzaja);

            Config.ConfigClass.SetProperty("ljetoDatumiOdDo", podatci.LjetoDatumiOdDo);
            Config.ConfigClass.SetProperty("zimaDatumiOdDo", podatci.ZimaDatumiOdDo);
            Config.ConfigClass.SetProperty("cijenaLjetoDan", podatci.CijenaLjetoDan.ToString());
            Config.ConfigClass.SetProperty("cijenaLjetoNoc", podatci.CijenaLjetoNoc.ToString());
            Config.ConfigClass.SetProperty("cijenaZimaDan", podatci.CijenaZimaDan.ToString());
            Config.ConfigClass.SetProperty("cijenaZimaNoc", podatci.CijenaZimaNoc.ToString());

        }

        internal static PodatciOTerenima GetPodatciOTerenimaFromConfigFile()
        {
            return new PodatciOTerenima()
            {

                AppShortName = ConfigClass.GetProperty("appShortName"),
                AppLongName = ConfigClass.GetProperty("appLongName"),
                KontaktMobitel = ConfigClass.GetProperty("kontaktMobitel"),
                KontaktEmail = ConfigClass.GetProperty("kontaktEmail"),
                AdresaObjekta = ConfigClass.GetProperty("adresaObjekta"),
                OpisObjekta = ConfigClass.GetProperty("courtsAddress1"),
                RadnoVrijemeOd = int.Parse(ConfigClass.GetProperty("radnoVrijemeOd")),
                RadnoVrijemeDo = int.Parse(ConfigClass.GetProperty("radnoVrijemeDo")),
                BrzinaCarousela = int.Parse(ConfigClass.GetProperty("brzinaCarousela")),
                SlikaPozadineSadrzaja = ConfigClass.GetProperty("slikaPozadineSadrzaja"),
                LjetoDatumiOdDo = ConfigClass.GetProperty("ljetoDatumiOdDo"),
                ZimaDatumiOdDo = ConfigClass.GetProperty("zimaDatumiOdDo"),
                CijenaLjetoDan = int.Parse(ConfigClass.GetProperty("cijenaLjetoDan")),
                CijenaLjetoNoc = int.Parse(ConfigClass.GetProperty("cijenaLjetoNoc")),
                CijenaZimaDan = int.Parse(ConfigClass.GetProperty("cijenaZimaDan")),
                CijenaZimaNoc = int.Parse(ConfigClass.GetProperty("cijenaZimaNoc")),
            };
        }

        internal static List<Pozadina> GetPozadine()
        {
            DirectoryInfo d = new DirectoryInfo($@"{HttpContext.Current.Server.MapPath("~")}\Images\Backgrounds");
            FileInfo[] Files = d.GetFiles("*.png");
            List<Pozadina> pozadine = new List<Pozadina>();
            foreach (FileInfo file in Files)
            {
                pozadine.Add(new Pozadina(file.Name.Substring(0, file.Name.IndexOf('.'))));
            }
            return pozadine;
        }


        internal static IEnumerable<Teren> GetSviTereni()
        {
            return Teren.ParseTereniFromConfigFile(ConfigClass.GetProperty("tereni")).ToList();
        }

        private static List<DanUTjednu> ParseRadniDani(string radniDani)
        {
            var values = Enum.GetValues(typeof(DanUTjednu));
            var list = new List<DanUTjednu>();

            foreach (var item in values)
            {
                if (radniDani.Contains(item.ToString()))
                {
                    list.Add((DanUTjednu)(Enum.Parse(typeof(DanUTjednu), item.ToString())));
                }
            }
            return list;
        }

        internal static IEnumerable<Teren> GetSlobodniTereni(DateTime datum, int vrijeme)
        {
            using (var db = new TennisContext())
            {
                var rezervacije = GetRezervacijeZaSat(datum, vrijeme);
                List<Teren> tereni = GetSviTereni().ToList();
                Console.WriteLine($"rezervacije: {rezervacije.Count}, tereni: {tereni.Count}");
                foreach (var rezervacija in rezervacije)
                {
                    tereni.Remove(new Teren(rezervacija.Teren));
                }

                return tereni.AsEnumerable();
            }
        }



        //---Osobni podatci---
        private static void DeleteOsobniPodatci()
        {
            //property can decide whether to delete or keep personal data
            if (bool.Parse(ConfigClass.GetProperty("doWeDeletePersonalData")) == true)
            {
                using (var db = new TennisContext())
                {
                    var rezervacije = db.Rezervacijas
                        .Where(c => DbFunctions.TruncateTime(c.DatumTermina) < DateTime.Today && c.OsobniPodatciIzbrisani == false)
                        .ToList();

                    foreach (var item in rezervacije)
                    {
                        item.ImeIPrezime = "[izbrisano]";
                        if (item.Email != null)
                        {
                            item.Email = "[izbrisano]";
                        }
                        item.BrojMobitela = "[izbrisano]";
                        item.OsobniPodatciIzbrisani = true;
                    }
                    db.SaveChanges();
                }
            }
        }
    }
}