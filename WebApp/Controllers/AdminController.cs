﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Web.Mvc;
using WebApp.Data;
using WebApp.Models;
using WebApp.Models.Utilities;

namespace WebApp.Controllers
{
    [Authorize(Roles = "Admin, Moderator")]
    public class AdminController : Controller
    {
        [HttpGet]
        public ActionResult Panel()
        {
            ViewBag.DatumOd = DateTime.Today;
            ViewBag.DatumDo = DateTime.Today.AddDays(2);
            ViewBag.Rezervacije = Repo.GetRezervacijeZaRangeDana(ViewBag.DatumOd, ViewBag.DatumDo);

            return View();
        }
        
        [HttpPost]
        public ActionResult Panel(DateTime DatumTerminaOd, DateTime DatumTerminaDo)
        {
            ViewBag.Rezervacije = Repo.GetRezervacijeZaRangeDana(DatumTerminaOd, DatumTerminaDo);
            ViewBag.DatumOd = DatumTerminaOd;
            ViewBag.DatumDo = DatumTerminaDo;
            return View();
        }


        [HttpGet]
        public ActionResult Reserve()
        {
            ViewBag.Termini = Repo.GetSlobodniTermini(DateTime.UtcNow, 1);
            ViewBag.Trajanja = Repo.GetTrajanja();
            return View();
        }

        [HttpPost]
        public ActionResult Reserve(DateTime DatumTermina, bool AllHours, int TrajanjeTerminaUSatima, int VrijemeTermina, string teren, String Notes)
        {
            var sb = new StringBuilder();

            if (AllHours == true)
            {
                sb.Append("This time slots have been occupied:");
                foreach (var termin in Repo.GetSlobodniTermini(DatumTermina))
                {
                    if (Repo.GetSlobodniTereni(DatumTermina, termin.TerminSat).ToList().Contains(new Teren(teren)))
                    {
                        sb.Append("\n" + Repo.AddZauzetoRezervacija(DatumTermina, termin.TerminSat, teren, Notes));
                    }
                }
            }
            else
            {
                sb.Append("This time slot has been occupied:");
                sb.Append("\n" + Repo.AddZauzetoRezervacija(DatumTermina, VrijemeTermina, teren, Notes));
            }

            ViewBag.Termini = Repo.GetSlobodniTermini(DateTime.UtcNow, 1);
            ViewBag.Trajanja = Repo.GetTrajanja();

            ViewBag.Message = sb.ToString();
            ViewBag.Added = true;
            return View();  
        }
        

        [ChildActionOnly]
        public ActionResult PregledRezervacija(DateTime DatumOd, DateTime DatumDo)
        {
            var rezervacije = Repo.GetRezervacijeZaRangeDana(DatumOd, DatumDo);
            ViewBag.DatumOd = DatumOd;
            ViewBag.DatumDo = DatumDo;
            return PartialView(rezervacije);
        }


        public ActionResult Canceled(DateTime? DatumOd = null, DateTime? DatumDo = null)
        {
            if (DatumOd == null ||DatumDo == null)
            {
                DatumOd = DateTime.Today;
                DatumDo = ((DateTime)DatumOd).AddDays(3);
            }
            DateTime _datumOd = (DateTime)DatumOd;
            DateTime _datumDo = (DateTime)DatumDo;

            var rezervacije = Repo.GetOtkazaneRezervacijeZaDan(_datumOd, _datumDo);
            ViewBag.DatumOd = _datumOd;
            ViewBag.DatumDo = _datumDo;
            return View(rezervacije);
        }

        [HttpGet]
        public ActionResult EditData()
        {
            ViewBag.Pozadine = Repo.GetPozadine();
            return View(Repo.GetPodatciOTerenimaFromConfigFile());
        }

        [HttpPost]
        public ActionResult EditData(PodatciOTerenima podatci)
        {
            ViewBag.Pozadine = Repo.GetPozadine();
            Repo.UrediPodatkeOTerenima(podatci);
            ViewBag.Updated = true;
            return View();
        }

        public ActionResult Recents(int howMany = 10)
        {
            ViewBag.HowMany = howMany;
            return View(Repo.GetPosljednjeRezervacije(howMany));
        }

        [HttpGet]
        public ActionResult Statistics()
        {
            return View();
        }

        [HttpGet]
        public ActionResult EditCourts()
        {
            ViewBag.Tereni = Repo.GetSviTereni();
            return View();
        }

        [HttpPost]
        public ActionResult EditCourts(String[] args)
        {
            var tereni = new List<Teren>();
            foreach (var item in args)
            {
                tereni.Add(new Teren(item));
            }
            ViewBag.Tereni = tereni;
            return View();
        }

        public ActionResult GetSlobodniTermini(DateTime date)
        {
            return Json(Repo.GetSlobodniTermini(date, 1), JsonRequestBehavior.AllowGet);
        }
    }
}