﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebApp.Config;
using WebApp.Data;
using WebApp.Models;

namespace WebApp.Controllers
{
    public class ReservationController : Controller
    {
        [HttpGet]  
        public ActionResult Index()
        {
            ViewBag.Termini = Repo.GetSlobodniTermini(DateTime.Today, 1);
            ViewBag.Trajanja = Repo.GetTrajanja();
            ViewBag.Datum = DateTime.Parse(DateTime.UtcNow.ToShortDateString());
            return View();
        }

        [HttpPost]
        public ActionResult Index(Rezervacija rezervacija)
        {
            Repo.AddRezervacija(rezervacija);
            return RedirectToAction("View", new { id = rezervacija.RezervacijaId, added = true });
        }

        [HttpGet]
        public ActionResult View(Guid id)
        {
            using (var db = new TennisContext())
            {
                var x = db.Rezervacijas.SingleOrDefault(b => b.RezervacijaId == id);
                return View(x);
            }
        }

        [HttpGet]
        public ActionResult Book()
        {
            if (!bool.Parse(ConfigClass.GetProperty("active")))
            {
                return RedirectToAction("Unavailable");
            }
            ViewBag.Termini = Repo.GetSlobodniTermini(DateTime.UtcNow, 1);
            ViewBag.Trajanja = Repo.GetTrajanja();
            ViewBag.Datum = DateTime.Parse(DateTime.UtcNow.ToShortDateString());
            ViewBag.DoWeDeletePersonalData = bool.Parse(ConfigClass.GetProperty("doWeDeletePersonalData"));
            return View();
        }
        
        [HttpGet]
        public ActionResult AddNotes(Guid id)
        {
            ViewBag.ReservationId = id;
            var _reservation = Repo.GetRezervacija(id);
            if(!String.IsNullOrEmpty(_reservation.Notes))
            {
                ViewBag.Notes = _reservation.Notes;
            }
            return View();
        }

        [HttpPost]
        public ActionResult AddNotes(Guid guid, String Notes)
        {
            var _reservation = Repo.GetRezervacija(guid);
            _reservation.Notes = Notes;
            Repo.UpdateReservation(_reservation, guid);
            return RedirectToAction("View", new { id = guid });
        }

        public ActionResult Unavailable()
        {
            return View();
        }

        public JsonResult GetActive()
        {
            return Json(ConfigClass.GetProperty("active"), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult Book(Rezervacija rezervacija)
        {
            Repo.AddRezervacija(rezervacija);
            return RedirectToAction("View", new { id = rezervacija.RezervacijaId });
        }

        public ActionResult Cancel(Guid id)
        {
            var rezervacija = Repo.GetRezervacija(id);
            Repo.OtkaziRezervaciju(rezervacija);
            return RedirectToAction(nameof(Deleted), new { id });
        }
        
        public ActionResult Deleted(Guid id)
        {
            ViewBag.Id = id;
            return View();
        }

        public ActionResult GetSlobodniTermini(DateTime date)
        {
            return Json(Repo.GetSlobodniTermini(date, 1), JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetSlobodniTereni(DateTime date, int time)
        {
            return Json(Repo.GetSlobodniTereni(date, time), JsonRequestBehavior.AllowGet);
        }
    }
}