﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace WebApp.Controllers
{
    public class InfoController : Controller
    {
        public ActionResult Prices()
        {
            return View();
        }

        public ActionResult Informations()
        {
            return View();
        }

        public ActionResult FAQ()
        {
            return View();
        }
    }
}