namespace WebApp.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class M_Occupied : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Rezervacijas", "Occupied", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Rezervacijas", "Occupied");
        }
    }
}
