namespace WebApp.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class M_init : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.InformacijeSustavas",
                c => new
                    {
                        InformacijeSustavaId = c.Int(nullable: false, identity: true),
                        PosljednjeAzuriranje = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.InformacijeSustavaId);
            
            CreateTable(
                "dbo.Rezervacijas",
                c => new
                    {
                        RezervacijaId = c.Guid(nullable: false),
                        ImeIPrezime = c.String(nullable: false),
                        BrojMobitela = c.String(nullable: false),
                        Email = c.String(),
                        DatumTermina = c.DateTime(nullable: false, storeType: "date"),
                        VrijemeTermina = c.Int(nullable: false),
                        TrajanjeTerminaUSatima = c.Int(nullable: false),
                        VrijemeRezerviranja = c.DateTime(nullable: false),
                        Otkazano = c.Boolean(nullable: false),
                        VrijemeOtkazivanja = c.DateTime(),
                        OsobniPodatciIzbrisani = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.RezervacijaId);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.Rezervacijas");
            DropTable("dbo.InformacijeSustavas");
        }
    }
}
