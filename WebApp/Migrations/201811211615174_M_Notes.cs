namespace WebApp.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class M_Notes : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Rezervacijas", "Notes", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Rezervacijas", "Notes");
        }
    }
}
