namespace WebApp.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class M_Translation : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Rezervacijas", "Email", c => c.String(nullable: false));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Rezervacijas", "Email", c => c.String());
        }
    }
}
