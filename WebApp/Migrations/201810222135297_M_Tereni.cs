namespace WebApp.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class M_Tereni : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Rezervacijas", "Teren", c => c.String(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Rezervacijas", "Teren");
        }
    }
}
