namespace WebApp.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class InformacijeSustava : DbMigration
    {
        public override void Up()
        {
            DropTable("dbo.InformacijeSustavas");
        }
        
        public override void Down()
        {
            CreateTable(
                "dbo.InformacijeSustavas",
                c => new
                    {
                        InformacijeSustavaId = c.Int(nullable: false, identity: true),
                        PosljednjeAzuriranje = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.InformacijeSustavaId);
            
        }
    }
}
