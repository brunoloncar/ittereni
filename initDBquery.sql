﻿--Open project
--Adjust config settings in Config/config.txt
--Ajust google maps HTML
--Adjust images

USE [itterenidb]
GO

INSERT INTO InformacijeSustavas VALUES (GETDATE())
INSERT INTO AspNetRoles VALUES (1, 'User'),(2, 'Admin'),(3, 'Moderator')

--Sample reservations
INSERT INTO Rezervacijas VALUES (NEWID(),'Zauzeto', '00000000', NULL, GETDATE(), 8, 1, GETDATE(), 0, NULL, 0)
INSERT INTO Rezervacijas VALUES (NEWID(),'Zauzeto', '00000000', NULL, GETDATE(), 9, 1, GETDATE(), 0, NULL, 0)
INSERT INTO Rezervacijas VALUES (NEWID(),'Zauzeto', '00000000', NULL, GETDATE(), 10, 1, GETDATE(), 0, NULL, 0)
INSERT INTO Rezervacijas VALUES (NEWID(),'Zauzeto', '00000000', NULL, GETDATE(), 11, 1, GETDATE(), 0, NULL, 0)
INSERT INTO Rezervacijas VALUES (NEWID(),'Zauzeto', '00000000', NULL, GETDATE(), 12, 1, GETDATE(), 0, NULL, 0)
INSERT INTO Rezervacijas VALUES (NEWID(),'Zauzeto', '00000000', NULL, GETDATE(), 13, 1, GETDATE(), 0, NULL, 0)
INSERT INTO Rezervacijas VALUES (NEWID(),'Zauzeto', '00000000', NULL, GETDATE(), 14, 1, GETDATE(), 0, NULL, 0)
INSERT INTO Rezervacijas VALUES (NEWID(),'Zauzeto', '00000000', NULL, GETDATE(), 15, 1, GETDATE(), 0, NULL, 0)

INSERT INTO Rezervacijas VALUES (NEWID(),'Zauzeto', '00000000', NULL, GETDATE(), 8, 1, GETDATE(), 0, NULL, 0)
INSERT INTO Rezervacijas VALUES (NEWID(),'Zauzeto', '00000000', NULL, GETDATE(), 9, 1, GETDATE(), 0, NULL, 0)
INSERT INTO Rezervacijas VALUES (NEWID(),'Zauzeto', '00000000', NULL, GETDATE(), 10, 1, GETDATE(), 0, NULL, 0)
INSERT INTO Rezervacijas VALUES (NEWID(),'Zauzeto', '00000000', NULL, GETDATE(), 11, 1, GETDATE(), 0, NULL, 0)
INSERT INTO Rezervacijas VALUES (NEWID(),'Zauzeto', '00000000', NULL, GETDATE(), 12, 1, GETDATE(), 0, NULL, 0)
INSERT INTO Rezervacijas VALUES (NEWID(),'Zauzeto', '00000000', NULL, GETDATE(), 13, 1, GETDATE(), 0, NULL, 0)
INSERT INTO Rezervacijas VALUES (NEWID(),'Zauzeto', '00000000', NULL, GETDATE(), 14, 1, GETDATE(), 0, NULL, 0)
INSERT INTO Rezervacijas VALUES (NEWID(),'Zauzeto', '00000000', NULL, GETDATE(), 15, 1, GETDATE(), 0, NULL, 0)

--Run the application
--REGISTER TO THE APP '{url}/account/register' WITH USERNAME 'admin@url' AND PASSWORD adminadmin1
--LOGOUT
--REGISTER TO THE APP '{url}/account/register' WITH USERNAME 'moderator@url' AND PASSWORD modmod1
--DISABLE REGISTER TO NON ADMIN USERS


INSERT INTO AspNetUserRoles VALUES ((SELECT Id FROM AspNetUsers WHERE Email LIKE 'admin@%'), 2)
INSERT INTO AspNetUserRoles VALUES ((SELECT Id FROM AspNetUsers WHERE Email LIKE 'moderator@%'), 3)
--IF NEEDED, ADD MORE ACCOUNTS AND GIVE THEM PROPER ROLES